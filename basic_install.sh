#!/usr/bin/zsh

# Arch Linux Install Script
# Features:
# Basics before installation
# Disk formatting
# LVM
# LUKS
# Pacstrap

#Partition Schema
#########################################################
#  EFI   #  BOOT  #   SWAP   #    /    #      HOME      #
#  550M  #   1G   #   16G    #   32G   #      100G      #
#########################################################
#  FAT32 #  ext4  #   SWAP   #   ext4  #      ext4      #  
#########################################################
#  not encrypted  #              encrypted              #
#########################################################


echo "Installation of Arch Linux started..."
echo "Internet connection recommended!"
echo "Setting up basics"

loadkeys de-latin1

if [ -d "/sys/firmware/efi/efivars" ] 
then
    echo "UEFI mode detected" 
else
    echo "Your system doesn't support EFI! Aborting..."
	exit 1
fi

timedatectl set-ntp true

#Partition disk
echo "Partitioning the disk..."
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | gdisk /dev/sda
  o 	# clear the in memory partition table
  y 	# apply
  n 	# new partition
  1 	# partition number 1 EFI
		# accept default start range
  +550m # add 550MB end range
  ef00  # add efi system
  n 	# new partition
  2 	# partition number 2 /boot
		# accept default start range
  +1G	# add 1GB end range
		# accept default fs type
  n 	# new partition
  3 	# partition number 3 REST OF HDD
		# accept default start range
		# accept default end range
		# accept default fs type
  w 	# write
  y 	# apply
EOF

#Encrypt
echo "Encrypting the disk..."
cryptsetup luksFormat --type luks2 /dev/sda3
cryptsetup open /dev/sda3 storage

#LVM
echo "Starting partitioning logically..."
pvcreate /dev/mapper/storage
vgcreate vg00 /dev/mapper/storage
lvcreate -L 16G vg00 -n swap
lvcreate -L 32G vg00 -n root
lvcreate -L 100G vg00 -n home

#FS
echo "Making file systems..."

mkfs.vfat -F32 /dev/sda1 	#/boot/efi
mkfs.ext4 /dev/sda2			#/boot

mkswap /dev/mapper/vg00-swap
mkfs.ext4 /dev/mapper/vg00-root
mkfs.ext4 /dev/mapper/vg00-home

#Mounting
echo "Mounting disks..."
mount /dev/mapper/vg00-root /mnt
mkdir /mnt/home
mount /dev/mapper/vg00-home /mnt/home
mkdir /mnt/boot
mount /dev/sda2 /mnt/boot
mkdir /mnt/boot/efi
mount /dev/sda1 /mnt/boot/efi

swapon /dev/mapper/vg00-swap

#Installing base system
echo "Now all is prepared for installation..."
echo "Starting installation ..."
pacstrap /mnt base

#Copy needed files
cp advanced_install.sh /mnt/root/extend_chroot.sh
chmod +x /mnt/root/extend_chroot

cp mkinitcpio.conf /mnt/etc/mkinitcpio.conf -f
cp grub /mnt/root/grub

genfstab -U /mnt > /mnt/etc/fstab

#Chroot
arch-chroot /mnt sh /root/extend_chroot.sh
