#!/bin/bash

# Arch Linux Install Script
# Features:
# Configure System and Bootloader


ln -sf /usr/share/zoneinfo/Europe/Vienna /etc/localtime

hwclock --systohc

echo "de_AT.UTF-8 UTF-8" > /etc/locale.gen
locale-gen

echo "LANG=de_DE.UTF-8" > /etc/locale.conf
echo "KEYMAP=de-latin1" > /etc/vconsole.conf


echo "Enter hostname: "
read host
echo $host > /etc/hostname

echo "127.0.0.1		localhost"
echo "::1			localhost"
echo $host"			localhost"

mkinitcpio -p linux

#Ethernet
echo "Description='Basic Eth Link'" > /etc/netctl/eth_dhcp
echo "Interface=eth0" >> /etc/netctl/eth_dhcp
echo "Connection=ethernet" >> /etc/netctl/eth_dhcp
echo "IP=dhcp" >> /etc/netctl/eth_dhcp

netctl enable eth_dhcp

#Bootloader
pacman -S grub efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=arch

echo "Now you have to do 4 steps on your own:"
echo "1. add cryptdevice=UUID=UUID_FROM_CRYPT:cryptroot to GRUB_CMDLINE_LINUX (/etc/default/grub)"
echo " "
echo "2. add 'lvm' to GRUB_PRELOAD_MODULES"
echo " "
echo "3. now execute grub-mkconfig -o /boot/grub/grub.cfg"
echo " "
echo "4. Check if fstab is okay with 'blkid" 
echo " "
echo "Shutdown your computer now, put the live disc out and boot your new fresh System." 
echo "For extended Features please run /root/final_install.sh after reboot" 
